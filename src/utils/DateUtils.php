<?php
namespace Fundiin\Payment\Utils;

use DateTime;
use Fundiin\Payment\Defines\Common;

class DateUtils {

    public static function validateDateIso8601($date, $format = "Y-m-d\TH:i:s.v\Z")
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public static function convertDateToDateIso8601(DateTime $datetime){
        return $datetime->format("Y-m-d\TH:i:s.v\Z");
    }
}
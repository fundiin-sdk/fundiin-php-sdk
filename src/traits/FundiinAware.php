<?php

namespace Fundiin\Payment\Traits;

use Fundiin\Payment\FundiinPayment;


trait FundiinAware
{
    /**
     * Sets the FundiinPayment instance on the child class
     * Used to later fetch the token, HTTP client, EntityFactory, etc
     * @param FundiinPayment $d
     * @return $this
     */
    public function registerFundiin(FundiinPayment $d)
    {
        $this->fundiin = $d;

        return $this;
    }
}
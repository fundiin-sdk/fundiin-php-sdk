<?php

namespace Fundiin\Payment\Abstracts;

use Fundiin\Payment\Traits\FundiinAware;
use Fundiin\Payment\Utils\ArrayKeyCaseConverter;
use GuzzleHttp\Exception\ClientException;

abstract class Api implements \Fundiin\Payment\Interfaces\Api
{
    /** @var int Timeout value in ms - defaults to 5s if empty */
    private $timeout = 30;

    /** @var string The URL onto which to unleash the API in question */
    private $url;
    protected $body;

    /** @var string API URL to which to send the request */
    protected $apiUrl;

    protected $fundiin;

    use FundiinAware;

    public function __construct($url, $body = null)
    {
        $this->url = $url;
        $this->body = $body;
    }

    public function setBody($body = null)
    {
        $this->body = $body;
    }

    public function setTimeout($timeout = null)
    {
        if ($timeout === null) {
            $timeout = 5;
        }

        if (!is_int($timeout)) {
            throw new \InvalidArgumentException('Parameter is not an integer');
        }

        if ($timeout < 0) {
            throw new \InvalidArgumentException('Parameter is negative. Only positive timeouts accepted.');
        }

        $this->timeout = $timeout;

        return $this;
    }

    public function callPostRequest()
    {
     
        $config = [
            "connect_timeout" => $this->timeout,
            "timeout" => $this->timeout,
            "verify" => false,
            "headers" => [
                'content-type' => 'application/json',
                'privateCode' => $this->fundiin->getToken()
            ]
        ];

        if ($this->body !== null ){
            if (is_array($this->body)){
                $normalizer = new ArrayKeyCaseConverter();
                $this->body = $normalizer->snakeCase($this->body);
            }
            
            $config["json"] = $this->body;
        }

        $result =  null;
        try {
            $response = $this->fundiin->getHttpClient()->request("POST", $this->url, $config);
            $result = json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            if ($e->getResponse() !== null) {
                $resBody =  $e->getResponse()->getBody()->getContents();
                if ($resBody !== null) {
                    $result = json_decode($resBody, true);
                }
            }
        }

        return $result;
    }
}

<?php
namespace Fundiin\Payment\Defines;

class Common {
    const DATETIME_ISO8601 = "Y-m-d\TH:i:s.v\Z";
}

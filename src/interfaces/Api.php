<?php

namespace Fundiin\Payment\Interfaces;


interface Api
{
    public function setTimeout($timeout = null);

    public function setBody($body = null);
    
    public function callPostRequest();
}
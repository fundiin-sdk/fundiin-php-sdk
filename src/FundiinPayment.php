<?php

namespace Fundiin\Payment;

use Fundiin\Payment\Apis\OnlineMerchantApi;
use Fundiin\Payment\Exceptions\FundiinException;
use GuzzleHttp\Client as GuzzleHttpClient;
use InvalidArgumentException;
use Http\Client\Common\HttpMethodsClient as Client;

class FundiinPayment
{
   protected static $token = null;
   protected static $merchantCode = null;
   protected static $env = "development";
   protected $instanceToken = null;
   protected $instanceMerchantCode = null;
   protected $client;
   private $FUNDIIN_HOST = "http://localhost:8084";

   private $API_MERCHANT_SEND_BOOKING_MESSAGE = "/fundiin/partner/{merchant_code}-send-booking-message";
   private $API_MERCHANT_CANCEL_BOOKING_MESSAGE = "/fundiin/partner/{merchant_code}-cancel-order";

   /**
    * @param null $token
    * @param null $merchantCode
    */
   public function __construct($token = null, $merchantCode = null)
   {
      if ($token === null && self::$token === null) {
         if (self::$token === null) {
            $msg = 'Vui lòng cung cấp merchant Token. ';
            $msg .= 'Sử dụng `FundiinPayment::setPrivateCodeAndMerchantCode`, hoặc khởi tạo instance FundiinPayment thông qua tham số $token.';
            throw new FundiinException($msg);
         } else {
            $this->instanceToken = self::$token;
         }
      }

      if ($merchantCode === null) {
         if (self::$merchantCode === null) {
            $msg = 'Vui lòng cung cấp merchantCode. ';
            $msg .= 'Sử dụng `FundiinPayment::setPrivateCodeAndMerchantCode`, hoặc khởi tạo instance FundiinPayment thông qua tham số $merchantCode.';
            throw new FundiinException($msg);
         } else {
            $this->instanceMerchantCode = self::$merchantCode;
         }
      }

      if ($token !== null && $merchantCode !== null) {
         $merchantCode = trim($merchantCode, " ");
         $merchantCode = strtolower($merchantCode);
         self::validateInput($token, $merchantCode);
         $this->instanceToken = $token;
         $this->instanceMerchantCode = $merchantCode;
      }

      $this->API_MERCHANT_SEND_BOOKING_MESSAGE = $this->FUNDIIN_HOST . str_replace("{merchant_code}", $this->instanceMerchantCode, $this->API_MERCHANT_SEND_BOOKING_MESSAGE);
      $this->API_MERCHANT_CANCEL_BOOKING_MESSAGE = $this->FUNDIIN_HOST . str_replace("{merchant_code}", $this->instanceMerchantCode, $this->API_MERCHANT_CANCEL_BOOKING_MESSAGE);
   }

   /**
    * @param mixed $token
    * @param mixed $merchantCode
    * 
    */
   public static function setPrivateCodeAndMerchantCode($token, $merchantCode)
   {
      if (self::validateInput($token, $merchantCode)) {
         self::$token = $token;

         $merchantCode = trim($merchantCode, " ");
         $merchantCode = strtolower($merchantCode);
         self::$merchantCode = $merchantCode;
      }
   }

   /**
    * @param mixed $env
    * 
    */
   public static function setEnviroment($env){
      if ($env !== null && $env === "production"){
         self::$env = $env;
      }
   }

   /**
    * @param mixed $token
    * @param mixed $merchantCode
    * 
    * @return [boolean]
    */
   private static function validateInput($token, $merchantCode)
   {
      if (!is_string($token)) {
         throw new InvalidArgumentException("Token is not a string");
      }

      if (strlen($token) <= 0) {
         throw new InvalidArgumentException("Token is not valid");
      }

      if (!is_string($merchantCode)) {
         throw new InvalidArgumentException("MerchantCode is not a string");
      }

      if (strlen($merchantCode) <=  0) {
         throw new InvalidArgumentException("MerchantCode is not valid");
      }

      return true;
   }

   /**
    * @return [string]
    */
   public function getToken()
   {
      return ($this->instanceToken) ? $this->instanceToken : self::$token;
   }

   /**
    * @return [string]
    */
   public function getMerchantCode()
   {
      return ($this->instanceMerchantCode) ? $this->instanceMerchantCode : self::$merchantCode;
   }

   /**
    * Sets the client to be used for querying the API endpoints
    *
    * @param Client $client
    * @see http://php-http.readthedocs.org/en/latest/utils/#httpmethodsclient
    * @return $this
    */
   public function setHttpClient(Client $client = null)
   {
      if ($client === null) {
         $client = new GuzzleHttpClient();
      }
      $this->client = $client;
      return $this;
   }

   /**
    * Returns either the instance of the Guzzle client that has been defined, or null
    * @return Client|null
    */
   public function getHttpClient()
   {
      return $this->client;
   }

   /**
    * @param string $shopId
    * @param string $orderId
    * @param string $phoneNumber
    * @param string $amount
    * 
    * @return array
    */
   public function sendBookingMessage(string $shopId, string $orderId, string $phoneNumber, string $amount): array
   {
      $api = new OnlineMerchantApi($this->API_MERCHANT_SEND_BOOKING_MESSAGE, null);
      if (!$this->getHttpClient()) {
         $this->setHttpClient();
      }
      /** Register fundiin object **/
      $registerApi =  $api->registerFundiin($this);
      /** Build request body **/
      $api->buildRequestBodySendBookingMessage($phoneNumber, $amount, $shopId, $orderId);
      /** Call request **/
      $response = $registerApi->callPostRequest();
      /** Return response **/
      return $response;
   }

   /**
    * @param string $shop_id
    * @param string $order_id
    * @param string $reason
    * @param int|null $refund_amount
    * 
    * @return array
    */
   public function cancelBooking(string $shop_id, string $order_id, string $reason = "", int $refund_amount = null): array
   {
      $api = new OnlineMerchantApi($this->API_MERCHANT_CANCEL_BOOKING_MESSAGE, null);
      if (!$this->getHttpClient()) {
         $this->setHttpClient();
      }
      /** Register fundiin object **/
      $registerApi =  $api->registerFundiin($this);
      /** Build request body **/
      $api->buildRequestBodyCancelBookingOrder($shop_id, $order_id, $reason, $refund_amount);
      /** Call request **/
      $response = $registerApi->callPostRequest();
      /** Return response **/
      return $response;
   }

}

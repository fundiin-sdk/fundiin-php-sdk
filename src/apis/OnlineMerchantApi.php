<?php

namespace Fundiin\Payment\Apis;

use DateTime;
use Fundiin\Payment\Abstracts\Api;
use Fundiin\Payment\Exceptions\FundiinException;

class OnlineMerchantApi extends Api
{

    public function buildRequestBodySendBookingMessage(string $phoneNumber, string $amount, string $shopId, string $orderId)
    {
        if ($phoneNumber === null || !is_string($phoneNumber) || strlen($phoneNumber) <= 0 || strlen($phoneNumber) >= 13) {
            throw new FundiinException("`phone_number` is invalid");
        }

        if ($amount === null || !is_string($amount) || strlen($amount) <= 0) {
            throw new FundiinException("`amount` is invalid");
        }

        if ((float) $amount <= 0) {
            throw new FundiinException("`amount` should be large than 1");
        }

        if ($shopId === null  || !is_string($shopId) || strlen($shopId) <= 0) {
            throw new FundiinException("`shop_id` is invalid");
        }

        if ($orderId === null  || !is_string($orderId) || strlen($orderId) <= 0) {
            throw new FundiinException("`order_id` is invalid");
        }

        $this->setBody([
            "phoneNumber" => $phoneNumber,
            "amount" => $amount,
            "shopId" => $shopId,
            "orderId" => $orderId
        ]);
    }

    public function buildRequestBodyCancelBookingOrder(string $shopId, string $orderId, string $reason = null, int $refundAmount=null)
    {

        if ($shopId === null  || !is_string($shopId) || strlen($shopId) <= 0) {
            throw new FundiinException("`shop_id` is invalid");
        }

        if ($orderId === null  || !is_string($orderId) || strlen($orderId) <= 0) {
            throw new FundiinException("`order_id` is invalid");
        }

         $this->setBody([
            "refundAmount" => $refundAmount,
            "reason" => $reason,
            "shopId" => $shopId,
            "orderId" => $orderId
        ]);
    }
}

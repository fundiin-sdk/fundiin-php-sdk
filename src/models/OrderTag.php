<?php

namespace Fundiin\Payment\Models;

use Fundiin\Payment\Exceptions\FundiinException;
use JsonSerializable;

class OrderTagBuilder
{
    private $shopId;
    private $orderId;
    private $tags;

    public function shopId(string $shopId)
    {
        $this->shopId = $shopId;
        return $this;
    }

    public function orderId(string $orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function tags(array $tags)
    {
        $this->tags = $tags;
        return $this;
    }

    public function getShopId()
    {
        return $this->shopId;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function build()
    {
        if ($this->getShopId() === null || !is_string($this->getShopId()) || strlen($this->getShopId()) <= 0){
            throw new FundiinException("Shop id không hợp lệ");
        }
        
        if ($this->getOrderId() === null || !is_string($this->getOrderId()) || strlen($this->getOrderId()) <= 0){
            throw new FundiinException("Order id không hợp lệ");
        }
        
        if ($this->getTags() === null || !is_array($this->getTags() || count($this->getTags())) < 0){
            throw new FundiinException("Tags không hợp lệ");
        }

        return new OrderTag($this);
    }
}

class OrderTag implements JsonSerializable
{
    private $shopId;
    private $orderId;
    private $tags;

    static function builder()
    {
        return new OrderTagBuilder();
    }

    public function __construct(OrderTagBuilder $orderTagBuilder = null)
    {
        if ($orderTagBuilder !== null) {
            $this->shopId = $orderTagBuilder->getShopId();
            $this->orderId = $orderTagBuilder->getOrderId();
            $this->tags = $orderTagBuilder->getTags();
        }
    }

    public function setShopId(string $shopId)
    {
        $this->shopId = $shopId;
    }

    public function setOrderId(string $orderId)
    {
        $this->orderId = $orderId;
    }

    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    public function getShopId()
    {
        return $this->shopId;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getTags()
    {
        return $this->tags;
    }


    public function jsonSerialize()
    {
        return
            [
                "shop_id" => $this->shopId,
                "order_id" => $this->orderId,
                "tags" => $this->tags
            ];
    }
}

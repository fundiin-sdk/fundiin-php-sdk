<?php

namespace Fundiin\Payment\Models;

use Fundiin\Payment\Exceptions\FundiinException;
use JsonSerializable;

class ProductItemBuilder
{
    private $id;
    private $productId;
    private $productName;
    private $quantity;
    private $totalPriceOriginal;
    private $totalPricePromotion;
    private $totalPriceOther;
    public function __construct()
    {
    }

    function id(string $id)
    {
        $this->id = $id;
        return $this;
    }

    function getId()
    {
        return $this->id;
    }

    function productId(string $productId)
    {
        $this->productId = $productId;
        return $this;
    }

    function getProductId()
    {
        return $this->productId;
    }

    function productName(string $productName)
    {
        $this->productName = $productName;
        return $this;
    }

    function getProductName()
    {
        return $this->productName;
    }

    function quantity(int $quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    function getQuantity()
    {
        return $this->quantity;
    }

    function totalPriceOriginal(int $totalPriceOriginal)
    {
        $this->totalPriceOriginal = $totalPriceOriginal;
        return $this;
    }

    function getTotalPriceOriginal()
    {
        return $this->totalPriceOriginal;
    }

    function totalPricePromotion(int $totalPricePromotion)
    {
        $this->totalPricePromotion = $totalPricePromotion;
        return $this;
    }

    function getTotalPricePromotion()
    {
        return $this->totalPricePromotion;
    }

    function totalPriceOther(int $totalPriceOther)
    {
        $this->totalPriceOther = $totalPriceOther;
        return $this;
    }

    function getTotalPriceOther()
    {
        return $this->totalPriceOther;
    }

    function build()
    {
        if ($this->getId() === null || !is_string($this->getId()) || strlen($this->getId()) <= 0){
            throw new FundiinException("Product item id không hợp lệ");
        }

        if ($this->getProductId() === null || !is_string($this->getProductId()) || strlen($this->getProductId()) <= 0){
            throw new FundiinException("Product id không hợp lệ");
        }

        if ($this->getProductName() === null || !is_string($this->getProductName()) || strlen($this->getProductName()) <= 0){
            throw new FundiinException("Product name không hợp lệ");
        }
        
        if ($this->getQuantity() === null || !is_numeric($this->getQuantity()) || $this->getQuantity() < 0){
            throw new FundiinException("Quantity không hợp lệ");
        }
        
        if ($this->getTotalPriceOriginal() === null || !is_numeric($this->getTotalPriceOriginal()) || $this->getTotalPriceOriginal() < 0){
            throw new FundiinException("Total price original không hợp lệ");
        }

        if ($this->getTotalPriceOther() === null || !is_numeric($this->getTotalPriceOther()) || $this->getTotalPriceOther() < 0){
            throw new FundiinException("Total price other không hợp lệ");
        }

        if ($this->getTotalPricePromotion() === null || !is_numeric($this->getTotalPricePromotion()) || $this->getTotalPricePromotion() < 0){
            throw new FundiinException("Total price promotion không hợp lệ");
        }
        
        return new ProductItem($this);
    }
}

class ProductItem implements JsonSerializable
{
    private $id;
    private $productId;
    private $productName;
    private $quantity;
    private $totalPriceOriginal;
    private $totalPricePromotion;
    private $totalPriceOther;

    static function builder()
    {
        return new ProductItemBuilder();
    }

    public function __construct(ProductItemBuilder $productItemBuilder = null)
    {
        if ($productItemBuilder !== null){
            $this->id = $productItemBuilder->getId();
            $this->productId = $productItemBuilder->getProductId();
            $this->productName = $productItemBuilder->getProductName();
            $this->quantity = $productItemBuilder->getQuantity();
            $this->totalPriceOriginal = $productItemBuilder->getTotalPriceOriginal();
            $this->totalPricePromotion = $productItemBuilder->getTotalPricePromotion();
            $this->totalPriceOther = $productItemBuilder->getTotalPriceOther();
        }
    }


    function setId(string $id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setProductId(string $productId)
    {
        $this->productId = $productId;
    }

    function getProductId()
    {
        return $this->productId;
    }

    function setProductName(string $productName)
    {
        $this->productName = $productName;
    }

    function getProductName()
    {
        return $this->productName;
    }

    function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    function getQuantity()
    {
        return $this->quantity;
    }

    function setTotalPriceOriginal(int $totalPriceOriginal)
    {
        $this->totalPriceOriginal = $totalPriceOriginal;
    }

    function getTotalPriceOriginal()
    {
        return $this->totalPriceOriginal;
    }

    function setTotalPricePromotion(int $totalPricePromotion)
    {
        $this->totalPricePromotion = $totalPricePromotion;
    }

    function getTotalPricePromotion()
    {
        return $this->totalPricePromotion;
    }

    function setTotalPriceOther(int $totalPriceOther)
    {
        $this->totalPriceOther = $totalPriceOther;
    }

    function getTotalPriceOther()
    {
        return $this->totalPriceOther;
    }

    public function jsonSerialize()
    {
        return
            [
                "id" => $this->id,
                "product_id" => $this->productId,
                "product_name" => $this->productName,
                "quantity" => $this->quantity,
                "total_price_original" => $this->totalPriceOriginal,
                "total_price_promotion" => $this->totalPricePromotion,
                "total_price_other" => $this->totalPriceOther
            ];
    }
}

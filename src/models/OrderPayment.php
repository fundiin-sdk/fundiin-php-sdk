<?php

namespace Fundiin\Payment\Models;

use Fundiin\Payment\Exceptions\FundiinException;
use JsonSerializable;

class OrderPaymentBuilder
{

    private $shopId;
    private $orderId;
    private $paymentStatus;
    private $series;

    public function getShopId()
    {
        return $this->shopId;
    }

    public function shopId(string $shopId)
    {
        $this->shopId = $shopId;
        return $this;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function orderId(string $orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    public function paymentStatus(int $paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
        return $this;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function series(array $series)
    {
        for ($i = 0, $size = count($series); $i < $size; ++$i) {
            if (!($series[$i] instanceof SeriesItem)) {
                throw new FundiinException("series item không phải instance của lớp SeriesItem");
            }
        }

        $this->series = $series;
        return $this;
    }

    public function build()
    {
        if ($this->getShopId() === null || !is_string($this->getShopId()) || strlen($this->getShopId()) <= 0) {
            throw new FundiinException("Giá trị shopId chưa hợp lệ");
        }

        if ($this->getOrderId() === null || !is_string($this->getOrderId()) || strlen($this->getOrderId()) <= 0) {
            throw new FundiinException("Giá trị orderId chưa hợp lệ");
        }

        if ($this->getPaymentStatus() === null || !is_numeric($this->getPaymentStatus()) || in_array($this->getPaymentStatus(), array(1, 2, 3, 4), true)) {
            throw new FundiinException("Giá trị paymentStatus chưa hợp lệ");
        }

        if (!in_array($this->getPaymentStatus(), array(2, 4), true)) {
            $this->series = array();
        }

        return new OrderPayment($this);
    }
}

class OrderPayment implements JsonSerializable
{
    private $shopId;
    private $orderId;
    private $paymentStatus;
    private $series;

    static function builder()
    {
        return new OrderPaymentBuilder();
    }

    public function __construct(OrderPaymentBuilder $orderPaymentBuilder = null)
    {
        if ($orderPaymentBuilder !== null) {
            $this->shopId = $orderPaymentBuilder->getShopId();
            $this->orderId = $orderPaymentBuilder->getOrderId();
            $this->paymentStatus = $orderPaymentBuilder->getPaymentStatus();
            $this->series = $orderPaymentBuilder->getSeries();
        }
    }

    public function getShopId()
    {
        return $this->shopId;
    }

    public function setShopId(string $shopId)
    {
        $this->shopId = $shopId;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function setOrderId(string $orderId)
    {
        $this->orderId = $orderId;
    }

    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(int $paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function setSeries(array $series)
    {
        for ($i = 0, $size = count($series); $i < $size; ++$i) {
            if (!($series[$i] instanceof SeriesItem)) {
                throw new FundiinException("series item không phải instance của lớp SeriesItem");
            }
        }

        $this->series = $series;
    }

    public function jsonSerialize()
    {
        return
            [
                "shop_id" => $this->shopId,
                "order_id" => $this->orderId,
                "payment_status" => $this->paymentStatus,
                "series" => $this->series
            ];
    }
}

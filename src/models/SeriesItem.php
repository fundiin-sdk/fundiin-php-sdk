<?php

namespace Fundiin\Payment\Models;

use DateTime;
use JsonSerializable;


class SeriesItem implements JsonSerializable
{
    private $id;
    private $amount;
    private $paymentDate;

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setPaymentDate(DateTime $paymentDate)
    {
        $this->paymentDate = $paymentDate;
    }

    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    public function jsonSerialize()
    {
        return
            [
                "id" => $this->id,
                "amount" => $this->amount,
                "payment_date" => $this->paymentDate
            ];
    }
}

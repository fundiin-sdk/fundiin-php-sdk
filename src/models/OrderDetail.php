<?php

namespace Fundiin\Payment\Models;

use DateTime;
use Fundiin\Payment\Defines\Common;
use JsonSerializable;
use Fundiin\Payment\Exceptions\FundiinException;
use Fundiin\Payment\Utils\DateUtils;

class OrderDetailBuilder
{
    private $code;
    private $message;
    private $orderId;
    private $recipientPhone;
    private $recipientName;
    private $shippingAddress;
    private $totalPriceProductItems;
    private $createdAt;
    private $paymentGateway;
    private $productItems;


    public function getCode()
    {
        return $this->code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getRecipientPhone()
    {
        return $this->recipientPhone;
    }

    public function getRecipientName()
    {
        return $this->recipientName;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function getTotalPriceProductItems()
    {
        return $this->totalPriceProductItems;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function getPaymentGateway()
    {
        return $this->paymentGateway;
    }

    public function getProductItems()
    {
        return $this->productItems;
    }

    public function __construct()
    {
    }

    function code(int $code)
    {
        $this->code = $code;
        return $this;
    }

    function message(string $message = null)
    {
        $this->message = $message;
        return $this;
    }

    function orderId(string $orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    function recipientPhone(string $recipientPhone = null)
    {
        $this->recipientPhone = $recipientPhone;
        return $this;
    }

    function recipientName(string $recipientName = null)
    {
        $this->recipientName = $recipientName;
        return $this;
    }

    function shippingAddress(string $shippingAddress = null)
    {
        $this->shippingAddress = $shippingAddress;
        return $this;
    }

    function totalPriceProductItems(int $totalPriceProductItems)
    {
        $this->totalPriceProductItems = $totalPriceProductItems;
        return $this;
    }

    function createdAt(Datetime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    function paymentGateway(string $paymentGateway = null)
    {
        $this->paymentGateway = $paymentGateway;
        return $this;
    }

    function productItems(array $productItems)
    {
        for ($i = 0, $size = count($productItems); $i < $size; ++$i) {
            if (!($productItems[$i] instanceof ProductItem)) {
                throw new FundiinException("product item không phải instance của lớp ProductItem");
            }
        }

        $this->productItems = $productItems;
        return $this;
    }

    function addProductItems(ProductItem $productItem)
    {
        if ($this->getProductItems() === null) {
            $this->productItems = array();
        }

        array_push($this->productItems, $productItem);
        return $this;
    }

    function build()
    {
        if (!$this->validateBuilder()) {
            throw new FundiinException("Validate fail");
        }

        return new OrderDetail($this);
    }

    private function validateBuilder(): bool
    {
        if ($this->getCode() === null || $this->getCode() === 0) {
            throw new FundiinException("`code` = 1 là success, [-1, -2 , -3] số âm is fail");
        }

        if ($this->getOrderId() === null || $this->getOrderId() <= 0) {
            throw new FundiinException("`orderId` không được trống");
        }

        if ($this->getTotalPriceProductItems() === null || $this->getTotalPriceProductItems() <= 0) {
            throw new FundiinException("`totalPriceProductItems` không được trống, nên bắt đầu từ 1");
        }

        if ($this->getCreatedAt() === null) {
            throw new FundiinException("`createdAt` không được trống");
        }

        if ($this->getProductItems() === null || count($this->getProductItems()) <= 0) {
            throw new FundiinException("`productItems` không được trống");
        }

        return true;
    }
}


class OrderDetail implements JsonSerializable
{
    private $code;
    private $message;
    private $orderId;
    private $recipientPhone;
    private $recipientName;
    private $shippingAddress;
    private $totalPriceProductItems;
    private $createdAt;
    private $paymentGateway;
    private $productItems;

    static function builder()
    {
        return new OrderDetailBuilder();
    }

    public function __construct(OrderDetailBuilder $orderDetailBuilder = null)
    {
        if ($orderDetailBuilder !== null) {
            $this->code = $orderDetailBuilder->getCode();
            $this->message = $orderDetailBuilder->getMessage();
            $this->orderId = $orderDetailBuilder->getOrderId();
            $this->recipientPhone = $orderDetailBuilder->getRecipientPhone();
            $this->recipientName = $orderDetailBuilder->getRecipientName();
            $this->shippingAddress = $orderDetailBuilder->getShippingAddress();
            $this->totalPriceProductItems = $orderDetailBuilder->getTotalPriceProductItems();
            $this->createdAt = $orderDetailBuilder->getCreatedAt();
            $this->paymentGateway = $orderDetailBuilder->getPaymentGateway();
            $this->productItems = $orderDetailBuilder->getProductItems();
        }
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function getRecipientPhone()
    {
        return $this->recipientPhone;
    }

    public function getRecipientName()
    {
        return $this->recipientName;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function getTotalPriceProductItems()
    {
        return $this->totalPriceProductItems;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function getPaymentGateway()
    {
        return $this->paymentGateway;
    }

    public function getProductItems()
    {
        return $this->productItems;
    }


    function setCode(int $code)
    {
        $this->code = $code;
    }

    function setMessage(string $message = null)
    {
        $this->message = $message;
    }

    function setOrderId(string $orderId)
    {
        $this->orderId = $orderId;
    }

    function setRecipientPhone(string $recipientPhone = null)
    {
        $this->recipientPhone = $recipientPhone;
    }

    function setRecipientName(string $recipientName = null)
    {
        $this->recipientName = $recipientName;
    }

    function setShippingAddress(string $shippingAddress = null)
    {
        $this->shippingAddress = $shippingAddress;
    }

    function setTotalPriceProductItems(int $totalPriceProductItems)
    {
        $this->totalPriceProductItems = $totalPriceProductItems;
    }

    function setCreatedAt(Datetime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    function setPaymentGateway(string $paymentGateway = null)
    {
        $this->paymentGateway = $paymentGateway;
    }

    function setProductItems(array $productItems)
    {
        for ($i = 0, $size = count($productItems); $i < $size; ++$i) {
            if (!($productItems[$i] instanceof ProductItem)) {
                throw new FundiinException("product item không phải instance của lớp ProductItem");
            }
        }

        $this->productItems = $productItems;
    }

    public function jsonSerialize()
    {
        return
            [
                "code" => $this->code,
                "message" => $this->message,
                "order_id" => $this->orderId,
                "recipient_phone" => $this->recipientPhone,
                "recipient_name" => $this->recipientName,
                "shipping_address" => $this->shippingAddress,
                "total_price_product_items" => $this->totalPriceProductItems,
                "created_at" => DateUtils::convertDateToDateIso8601($this->createdAt),
                "payment_gateway" => $this->paymentGateway,
                "product_items" => $this->productItems
            ];
    }
}

# Cách sử dụng Fundiin SDK dành cho PHP
<h3>REQUIREMENT:</h3>
- PHP: ^5.6|7.* <br/>
- Composer: v1 <br/>

<h3>Setup</h3>

1. Token và merchant code:

```        
FundiinPayment::setPrivateCodeAndMerchantCode(TOKEN, MERCHANT_CODE);
```

2. Enviroment:

```
FundiinPayment::setEnviroment("production");
```
---

### 1. MERCHANT Gửi lệnh booking đơn hàng
- Sử dụng:
```
$fundiin = new FundiinPayment($TOKEN, $MERCHANT_CODE);
$response = $fundiin->sendBookingMessage($shopId, $orderId, $phoneNumber, $amount);
```

- Kết quả:
  - Loại: array
  - Dữ liệu:
```
{
    "code": 1,
    "message"": "Thành Công"
}
```

![alt text](imgs/error_code_send_booking_message.png "Title")

### 2. MERCHANT Huỷ lệnh booking đơn hàng
- Sử dụng:
```
$fundiin = new FundiinPayment($OKEN, $MERCHANT_CODE);
$response = $fundiin->cancelBooking($shopId, $orderId, $reason, $refundAmount);
```
- Kết quả:
  - Loại: array
  - Dữ liệu:
```
{
    "code": 1,
    "message"": "Thành Công"
}				
```
![alt text](imgs/error_code_cancel_booking_message.png "Title")

### 3. Model Chi tiết đơn hàng:
- Note: Trong model đã có implement <b>JsonSerializable</b> để chuyển đổi keys thành <b>snakeCase</b>. 

- <h4><u><b> Product Item </b></u></h4>
```
$productItem = ProductItem::builder() 
                    ->id(1)                         //  int     | required
                    ->productId("1")                //  string  | required
                    ->productName("abc")            //  string  | required
                    ->quantity(1)                   //  int     | required
                    ->totalPriceOriginal(100)       //  long    | required
                    ->totalPricePromotion(10)       //  long    | required
                    ->totalPriceOther(90)           //  long    | required
                    ->build();
```
- <h4><u><b> Order Detail </b></u></h4>
```
$orderDetailBuilder = OrderDetail::builder()
    ->code(1)                                   //  int     | required
    ->message(null)                             //  string  
    ->recipientPhone(null)                      //  string  
    ->recipientName(null)                       //  string
    ->orderId(1)                                //  string  | required
    ->totalPriceProductItems(100)               //  long    | required    
    ->createdAt($result)                        // datetime | required | format: iso8601
    ->shippingAddress(null)                     // string   | required
    ->paymentGateway(null)                      // string   | required

-- thêm product item có 2 phương thức: 
    1. $orderDetailBuilder->productItems(array($productItem1, $productItem2));   // []ProductItem   | required  
    2. $orderDetailBuilder->addProductItems($productItem)                        // ProductItem     | required

-- build:
$orderDetail = $orderDetailBuilder->build();
```
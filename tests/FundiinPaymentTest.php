<?php

namespace Fundiin\Payment\Test;

use DateTime;
use Fundiin\Payment\FundiinPayment;
use Fundiin\Payment\Models\OrderDetail;
use Fundiin\Payment\Models\ProductItem;
use PHPUnit\Framework\TestCase;

/**
 * ./vendor/bin/phpunit --exclude-group ignore tests
 */
class FundiinPaymentTest extends TestCase
{
    private $TOKEN = '2?AHyA9NZPEGBE$jN&lKLVWwT%$H?^kB#oYV6IaujK%JQlNBQL';
    private $MERCHANT_CODE = "HARAVAN";
    private $SHOPID = '200000165745';

    
    public function testSendPreBookingSmsCaseCreateInstance()
    {
        $fundiin = new FundiinPayment($this->TOKEN, $this->MERCHANT_CODE);
        $response = $fundiin->sendBookingMessage($this->SHOPID, "12345677", "0358397956", "1000000");
        var_dump($response);
        assert(1, $response["code"]);
    }

    /**
     * Remove this line to test
     * @group ignore
     */
    public function testSendPreBookingSmsCaseStatic()
    {
        FundiinPayment::setPrivateCodeAndMerchantCode($this->TOKEN, $this->MERCHANT_CODE);
        $fundiin = new FundiinPayment();
        $response = $fundiin->sendBookingMessage("200000165745", "123456789", "0358397956", "1000000");
        var_dump($response);
        assert(1, $response["code"]);
    }

    /**
     * Remove this line to test
     * @group ignore
     */
    public function testCancelPreBookingSmsCaseCreateInstance()
    {
        # code...
        $fundiin = new FundiinPayment($this->TOKEN,  $this->MERCHANT_CODE);
        $response = $fundiin->cancelBooking("200000165745", "12345678");
        var_dump($response);
        assert(1, $response["code"]);
    }

/**
     * Remove this line to test
     * @group ignore
     */
    public function testBuilderOrderDetail()
    {
        $createAt = new DateTime();
        $productItem = ProductItem::builder()->id(1)->productId(1)->productName(1)->quantity(1)->totalPriceOriginal(10000000000000)->totalPricePromotion(100000000)->totalPriceOther(900000000)->build();
        $orderDetail = OrderDetail::builder()->code(1)->orderId(1)->totalPriceProductItems(100)->createdAt($createAt)->productItems(array($productItem))->build();
        echo json_encode($orderDetail);
        $this->assertInstanceOf(OrderDetail::class, $orderDetail);
    }
/**
     * Remove this line to test
     * @group ignore
     */
    public function testBuilderOrderPayment()
    {
        $createAt = new DateTime();

        $productItemBuilder = ProductItem::builder()->id(1)->productId(1)->productName(1)->quantity(1)->totalPriceOriginal(10000000000)->totalPricePromotion(100000)->totalPriceOther(900000000);
        $productItem = $productItemBuilder->build();

        $orderDetailBuilder = OrderDetail::builder()
            ->code("0")
            ->message(null)
            ->recipientName(null)
            ->recipientPhone(null)
            ->orderId(1)
            ->totalPriceProductItems(100)
            ->createdAt($createAt)
            ->shippingAddress(null)
            ->paymentGateway('fundiin');
        
        
        $orderDetailBuilder->productItems(array($productItem));
        $orderDetailBuilder->addProductItems($productItem);

        $orderDetail = $orderDetailBuilder->build();

        echo json_encode($orderDetail);
        $this->assertInstanceOf(OrderDetail::class, $orderDetail);
    }
}
